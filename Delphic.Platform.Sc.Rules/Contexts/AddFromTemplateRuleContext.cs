﻿using Sitecore;
using Sitecore.Diagnostics;
using Sitecore.Pipelines.ItemProvider.AddFromTemplate;
using Sitecore.Web.UI.Sheer;

namespace Delphic.Platform.Sc.Rules.Contexts
{
    public class AddFromTemplateRuleContext : Sitecore.Rules.RuleContext, IPipelineArgsRuleContext<AddFromTemplateArgs>
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AddFromTemplateRuleContext"/> class.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        public AddFromTemplateRuleContext([NotNull] AddFromTemplateArgs args)
        {
            Assert.IsNotNull(args, "args");
            Args = args;
            Item = args.ProcessorItem.InnerItem;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the args.
        /// </summary>
        [NotNull]
        public AddFromTemplateArgs Args { get; private set; }

        #endregion
    }
}
