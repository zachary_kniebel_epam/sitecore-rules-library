﻿using System;
using System.Runtime.InteropServices;
using Delphic.Platform.Sc.Rules.Conditions.Base;
using Sitecore;
using Sitecore.Rules;

namespace Delphic.Platform.Sc.Rules.Conditions.ItemHierarchy
{
    /// <summary>
    /// Rule condition to check if the item is a Media item
    /// </summary>
    /// <typeparam name="TRuleContext"></typeparam>
    [UsedImplicitly]
    [Guid("49544ED3-CAF3-45B6-9CEC-0E0B81F65858")]
    public sealed class ItemIsMediaItem<TRuleContext> : WhenCondition<TRuleContext>
        where TRuleContext : RuleContext
    {
        /// <summary>
        /// The execute rule.
        /// </summary>
        /// <param name="ruleContext">
        /// The rule context.
        /// </param>
        /// <returns>
        /// <c>True</c>, if the condition succeeds, otherwise <c>false</c>.
        /// </returns>
        protected override bool ExecuteRule(TRuleContext ruleContext)
        {
            var item = ruleContext.Item;
            return item != null && item.Paths.IsMediaItem;
        }
    }
}
