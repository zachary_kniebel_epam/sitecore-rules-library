﻿using System;
using System.Runtime.InteropServices;
using Delphic.Platform.Sc.Rules.Conditions.Base;
using Sitecore;
using Sitecore.Rules;

namespace Delphic.Platform.Sc.Rules.Conditions.ItemHierarchy
{
    /// <summary>
    /// Rule condition to check if the item is a content item
    /// </summary>
    /// <typeparam name="TRuleContext"></typeparam>
    [UsedImplicitly]
    [Guid("36C9B3DB-02D5-4D79-AC2C-EC5A5410D2F2")]
    public sealed class ItemIsContentItem<TRuleContext> : WhenCondition<TRuleContext>
        where TRuleContext : RuleContext
    {
        /// <summary>
        /// The execute rule.
        /// </summary>
        /// <param name="ruleContext">
        /// The rule context.
        /// </param>
        /// <returns>
        /// <c>True</c>, if the condition succeeds, otherwise <c>false</c>.
        /// </returns>
        protected override bool ExecuteRule(TRuleContext ruleContext)
        {
            var item = ruleContext.Item;
            return item != null && item.Paths.IsContentItem;
        }
    }
}
