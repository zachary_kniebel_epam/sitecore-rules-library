﻿using System;
using System.Runtime.InteropServices;
using Delphic.Platform.Sc.Rules.Conditions.Base;
using Sitecore;
using Sitecore.Data;
using Sitecore.Diagnostics;
using Sitecore.Rules;

namespace Delphic.Platform.Sc.Rules.Conditions.ItemInformation
{
    /// <summary>
    /// Rule that checks if the item was created from a branch template
    /// </summary>
    /// <typeparam name="TRuleContext"></typeparam>
    [UsedImplicitly]
    [Guid("F911896A-2A35-4B7C-BDF3-357B43DD7519")]
    public sealed class ItemCreatedFromAnyBranch<TRuleContext> : WhenCondition<TRuleContext>
        where TRuleContext : RuleContext
    {
        /// <summary>
        /// The execute rule.
        /// </summary>
        /// <param name="ruleContext">
        /// The rule context.
        /// </param>
        /// <returns>
        /// <c>True</c>, if the condition succeeds, otherwise <c>false</c>.
        /// </returns>
        protected override bool ExecuteRule(TRuleContext ruleContext)
        {
            var item = ruleContext.Item;
            return item != null && item.BranchId != ID.Null;
        }
    }
}
