﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sitecore;
using Sitecore.Diagnostics;
using Sitecore.Rules;

namespace Delphic.Platform.Sc.Rules.Actions.Base
{
    public abstract class RuleAction<TRuleContext> : Sitecore.Rules.Actions.RuleAction<TRuleContext>
        where TRuleContext : RuleContext
    {
        /// <summary>
        /// Action implementation.
        /// </summary>
        /// <remarks>
        /// Credit to Jim "Jimbo" Baltika for developing this class
        /// </remarks>
        /// <param name="ruleContext">
        /// The rule context.
        /// </param>
        public override void Apply([NotNull] TRuleContext ruleContext)
        {
            Assert.IsNotNull(ruleContext, "ruleContext");
            Assert.IsNotNull(ruleContext.Item, "ruleContext.Item");
            if (ruleContext.IsAborted)
            {
                return;
            }

            try
            {
                var msg = "RuleAction " + GetType().Name + " started for " + ruleContext.Item.Name;

                Trace.TraceInformation(msg);

                ApplyRule(ruleContext);
            }
            catch (Exception exception)
            {
                var msg = "RuleAction " + GetType().Name + " failed.";
                Log.Error(msg, exception, this);
                Trace.TraceError(msg);
            }

            var message = "RuleAction " + GetType().Name + " ended for " + ruleContext.Item.Name;

            Trace.TraceInformation(message);
        }

        /// <summary>
        /// The apply rule.
        /// </summary>
        /// <param name="ruleContext">
        /// The rule context.
        /// </param>
        protected abstract void ApplyRule([NotNull] TRuleContext ruleContext);
    }
}
